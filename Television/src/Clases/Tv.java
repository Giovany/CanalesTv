package Clases;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Giovany on 6/07/2017.
 */
public class Tv {

    private String marca;
    private int pulgadas;
    private String tipo;
    private List<Canal> canales;

    public Tv() {

        canales = new ArrayList<>();
    }


    public void addCanal(Canal canal){

        this.canales.add(canal);
    }

    public void canalPermitido(int numeroCanal){

        for (Canal c: canales){

            if (c.getNumeroCanal() == numeroCanal){
                  if (c.getPermitido() == "si"){
                      System.out.println("Canal " + c.getNumeroCanal() + " Permitido " + c.getTematica());
                  }

                  if (c.getPermitido() == "no"){
                     System.out.println("Canal  " + c.getNumeroCanal() + " No Permitido " + c.getTematica());

                  }
            }


        }


    }

}


