package Clases;

/**
 * Created by Giovany on 6/07/2017.
 */
public class Canal {

    private int numeroCanal;
    private String tematica;
    private String permitido;


    public Canal(int numeroCanal, String tematica, String permitido) {
        this.numeroCanal = numeroCanal;
        this.tematica = tematica;
        this.permitido = permitido;
    }


    public int getNumeroCanal() {
        return numeroCanal;
    }

    public void setNumeroCanal(int numeroCanal) {
        this.numeroCanal = numeroCanal;
    }

    public String getTematica() {
        return tematica;
    }

    public void setTematica(String tematica) {
        this.tematica = tematica;
    }

    public String getPermitido() {
        return permitido;
    }

    public void setPermitido(String permitido) {
        this.permitido = permitido;
    }
}
