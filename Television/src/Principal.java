import Clases.Canal;
import Clases.Tv;

/**
 * Created by Giovany on 6/07/2017.
 */
public class Principal {

    public static void main(String[] args) {

        Tv tele = new Tv();
        tele.addCanal(new Canal(11,"Deporte","no"));
        tele.addCanal(new Canal(12,"Series","no"));
        tele.addCanal(new Canal(13,"Dibujos Animados","si"));
        tele.addCanal(new Canal(14,"Series","no"));
        tele.addCanal(new Canal(15,"Deporte","no"));
        tele.addCanal(new Canal(16,"Deporte","no"));
        tele.addCanal(new Canal(17,"Dibujos Animados","si"));
        tele.addCanal(new Canal(18,"Deporte","no"));
        tele.addCanal(new Canal(19,"Dibujos Animados","si"));
        tele.addCanal(new Canal(20,"Series","no"));

        tele.canalPermitido(13);

        tele.canalPermitido(11);




    }
}
